<!-- To reduce spam, new users have reduced privileges.
     Please use this template to request more privileges, such as
     project creation and forking. -->

I want to contribute to projects here, so please add me to the list of internal users.

<!-- Summarize here why you are requesting access.
     For example: "I'm planning to work on adding feature A to project B"
     or "I'm planning to work on fixing issue 1234" -->

I'm planning to work on ...


## Checklist   <!-- Fill the square-brackets with an `x` if you agree with those points -->

<!-- You should read WineHQ's contributor policies before agreeing to the following.
     (To open it, either "preview" this message and then click/tap on the link below,
     or type/copy the link into the address bar of your web browser.) -->

- [ ] I understand that not abiding by the [WineHQ contributor
      policies](https://gitlab.winehq.org/winehq/winehq/-/wikis/home#contributor-policies)
      can result in the removal of previously granted privileges.
- [ ] I understand that posting spam will result in immediate
      suspension and/or deletion of my account.


/label ~"user verification"
